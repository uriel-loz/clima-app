import {readInput, inquirerMenu, pause, listPlaces} from './helpers/inquirer.js';
import Searches from './models/searches.js';
import dotenv from 'dotenv'
dotenv.config()

const main = async() => {
    const searches = new Searches();
    let opt = '';

    do {
        opt =  await inquirerMenu();

        if( opt == 1 ) {
            const place = await readInput( 'City: ' );
            const coincidences = await searches.city( place );
            const id = await listPlaces( coincidences );

            if ( id === '0' ) continue;

            const selectedPlace = coincidences.find( x  =>  x.id == id );

            searches.addHistory( selectedPlace.name );

            const weatherPlace = await searches.weatherPlace( selectedPlace.lat, selectedPlace.lng );

            console.log( '\nInformation of the city\n'.green );
            console.log( 'City:', selectedPlace.name );
            console.log( 'Lat:', selectedPlace.lat );
            console.log( 'Lng:', selectedPlace.lng );
            console.log( 'Temperature: ', weatherPlace.temp );
            console.log( 'Minimum:', weatherPlace.min );
            console.log( 'Maximum:', weatherPlace.max );
            console.log( "What's the weather like: ", weatherPlace.desc );
        } else if ( opt == 2 ) {
            searches.historialCapitalize.forEach( ( place, i ) => {
                const index = `${ i + 1 }.`.green;

                console.log( `${ index } ${ place } ` );
            });
        } // end if else

        if( opt !== 0 ) await pause();

    } while ( opt !== 0 );
}

main();