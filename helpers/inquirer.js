import inquirer from 'inquirer';
import colors from 'colors';

const questions = [
    {
        type: 'list',
        name: 'option',
        message: 'What do you want to do?',
        choices: [
            {
                value: 1,
                name: `${'1.'.green} City search`
            },
            {
                value: 2,
                name: `${'2.'.green} History`
            },
            {
                value: 0,
                name: `${'0.'.green} Exit`
            }
        ]
    }
];

export const inquirerMenu = async () => {
    console.clear();
    console.log('=========================='.green);
    console.log('  Select an option'.white);
    console.log('==========================\n'.green);

    const { option } = await inquirer.prompt(questions);

    return option;
}

export const listPlaces = async( places = [] ) => {
    const choices = places.map( ( place, i ) => {
        let index = `${i + 1}.`.green;

        return {
            value: place.id,
            name: `${index} ${place.name}`,
        }
    } );

    choices.unshift(
        {
            value: '0',
            name: '0.'.green + ' Cancel',
        }
    );

    const questions = [
        {
            type: 'list',
            name: 'id',
            message: 'Select place:',
            choices
        }
    ];

    const { id } = await inquirer.prompt(questions);

    return id;
}

export const readInput = async( message ) => {
    const question = [
        {
            type: 'input',
            name: 'description',
            message,
            validate( value ) {
                if ( value.length === 0 ) {
                    return 'Please enter a value';
                }
                return true;
            }
        }
    ];

    const { description } = await inquirer.prompt( question );

    return description;
}

export const showCheckList = async( tasks = [] ) => {
    const choices = tasks.map( ( task, i ) => {
        let index = `${i + 1}.`.green;

        return {
            value: task.id,
            name: `${index} ${task.description}`,
            checked: task.completed ? true : false
        }
    } );

    const question = [
        {
            type: 'checkbox',
            name: 'ids',
            message: 'Selections',
            choices
        }
    ];

    const { ids } = await inquirer.prompt(question);

    return ids;
}

export const confirm = async( message ) => {
    const question = [
        {
            type: 'confirm',
            name: 'ok',
            message
        }
    ];

    const { ok } = await inquirer.prompt(question);

    return ok;
}

const question = [
    {
        type: 'input',
        name: 'enter',
        message: `Press ${'ENTER'.green} to continue`
    }
];

export const pause = async () => {

    console.log('\n');
    await inquirer.prompt(question);
}