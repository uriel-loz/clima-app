import fs from 'fs';

import axios from 'axios';

class Searches {
    historyPlaces = [];
    dbPath = './db/database.json';

    constructor() {
        this.readDB().then(() => {
            console.log('Database read successfully.');
        });
    }

    get historialCapitalize() {
        return this.historyPlaces.map( place => {

            let words = place.split(' ');
            words = words.map( p => p[0].toUpperCase() + p.substring(1) );

            return words.join(' ')

        })
    }

    get paramsMapbox() {
        return {
            'access_token': process.env.MAPBOX_KEY
        };
    }

    get paramsOpenWeather() {
        return {
            appid: process.env.OPENWEATHER_KEY,
            units: 'metric'
        }
    }

    async city ( place = '' ) {    
        try {
            // console.log( place );
            const instance = axios.create({
                baseURL: `https://api.mapbox.com/geocoding/v5/mapbox.places/${place}.json`,
                params: this.paramsMapbox
            });

            const response = await instance.get();
            
            return response.data.features.map( place => ( {
                id: place.id,
                name: place.place_name,
                lng: place.center[ 0 ],
                lat: place.center[ 1 ]
            } ) );
            
        } catch (error) {
            return []; // return the places
        }
    }

    async weatherPlace ( lat, lon ) {
        try {
            // console.log( place );
            const instance = axios.create({
                baseURL: 'https://api.openweathermap.org/data/2.5/weather',
                params: {...this.paramsOpenWeather, lat, lon}
            });

            const response = await instance.get();
            
            return {
                desc: response.data.weather[ 0 ].description,
                min: response.data.main.temp_min,
                max: response.data.main.temp_max,
                temp: response.data.main.temp
            }
            
        } catch (error) {
           console.log(error);
        }
    }

    addHistory( place = '' ) {
        if( this.historyPlaces.includes( place.toLocaleLowerCase() ) ) 
            return;

        this.historyPlaces.unshift( place.toLocaleLowerCase() );

        this.saveDB();
    }

    saveDB = () => {
        const payload = {
            history: this.historyPlaces,
        };

        fs.writeFileSync( this.dbPath, JSON.stringify( payload ) );
    }
    
    readDB = async () => {
        if ( !fs.existsSync( this.dbPath ) ) return;
    
        const info = fs.readFileSync( this.dbPath, { encoding: 'utf-8' } );

        const data = JSON.parse( info );
        
        this.historyPlaces = data.history;
    }
}

export default Searches;